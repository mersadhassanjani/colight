FROM ubuntu:16.04

# c++ dependencies
RUN apt update && \
    apt-get install -y build-essential cmake wget git

# install Miniconda Python 3.6
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN wget -P /tmp/ https://repo.anaconda.com/miniconda/Miniconda3-py37_4.8.3-Linux-x86_64.sh && \
    /bin/bash /tmp/Miniconda3-py37_4.8.3-Linux-x86_64.sh -b -p /opt/conda && \
    rm /tmp/Miniconda3-py37_4.8.3-Linux-x86_64.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc

# install cityflow
COPY . /home/cityflow
RUN pip install flask && \
    cd /home/cityflow && \
    pip install .
RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
     libglib2.0-0 libxext6 libsm6 libxrender1 \
     git mercurial subversion
RUN pip install --upgrade pip
RUN apt-get install -y software-properties-common && \
    add-apt-repository ppa:sumo/stable && \
    apt-get update && \
    apt-get install -y sumo sumo-tools sumo-doc && \
    pip install msgpack && \
    pip install numpy && \
    pip install tensorflow==2.5.1 && \
    pip install tensorflow-estimator==2.5.1 && \
    pip install keras==2.1 && \
    pip install networkx && \
    pip install pandas && \
    pip install matplotlib

